"use strict";

class Scenery {
  constructor(name, listItems) {
    this.name = name;
    this.listItems = listItems;
    this.clue = setClue(this.listItems);
  }
}