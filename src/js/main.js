"use strict";

function getImage(image){
  return `url(./images/${image}.png)`;
}

function   setClue(listItems) {
  for (let i = 0; i < listItems.length; i++) {
    let item = listItems[i]["clue"];
    if (item === true) {
      return (item);
    }
  }
}

let listItem = [];
let ch = "character";
let it = "item";
let pl = "place";

let description = new SceneryItem("description");

let pascal = new SceneryItem("pascal", [description, description, description], ch); 
let eloise = new SceneryItem("eloise", [description], ch, true);

let knife = new SceneryItem("couteau",[description, description], it);

let cityHall = new SceneryItem("mairie",[description], pl);

listItem.push(cityHall);
listItem.push(pascal);
listItem.push(knife);
listItem.push(eloise);

let scene1 = new Scenery("meurtre à la mairie", listItem);

console.log(scene1);

let scenery = document.querySelector("#scenery");
for (let i = 0; i < scene1["listItems"].length; i++) {
  let div = document.createElement("div");
  div.setAttribute("id", scene1["listItems"][i]["name"]);
  div.setAttribute("class", "scenery-item col-md-3");
  div.style.backgroundImage = getImage(scene1["listItems"][i]["name"]);
  div.style.height = "50vh";
  scenery.appendChild(div);
}