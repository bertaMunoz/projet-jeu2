"use strict";

class SceneryItem {
  constructor(name, kind, description= null, clue = false){
    this.name = name;
    this.image = getImage(name);
    this.kind = kind;
    this.clue = clue;
    this.description = description;
  }
}